import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios'

Vue.config.productionTip = false

axios.defaults.baseURL = 'http://10.0.0.2:5000';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.withCredentials = true;  
Vue.prototype.$axios = axios;



new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

