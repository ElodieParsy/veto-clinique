import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    id_client:false,
    type: null,
  },
  mutations: {
    SET_CLIENT_META(state, user){
      state.id_client = user.id_personne;
      state.type = user.type;
      if (user.type == "veto"){
        state.nom = user.nom_personne;
        state.prenom = user.prenom_personne;
        state.avatar = user.avatar;
        state.specialite = user.specialite;
        state.mail = user.mail;
        state.tel = user.tel;
        state.sexe = user.sexe;
      }      
    }
  },
  actions: {
  },
  modules: {
  }
})
