import Vue from 'vue'
import VueRouter from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Equipes from '../views/Equipes.vue'
import About from '../views/About.vue'
import Rdv from '../views/Rdv.vue'
import Login from '../views/Login.vue'
import Compte from '../views/Compte.vue'
import Profil from '../views/Profil.vue'
import ProfilAnimals from '../views/ProfilAnimals.vue'
import ProfilRdv from '../views/ProfilRdv.vue'
import Register from '../views/Register.vue'
import VetoPlanning from '../views/VetoPlanning'
import store from '../store'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Accueil',
    component: Accueil
  },
  {
    path: '/veto-planning',
    name: 'AccueilVeto',
    component: VetoPlanning
  },
  {
    path: '/notre-equipe',
    name: 'Equipes',
    component: Equipes
  },
  {
    path: '/notre-histoire',
    name: 'About',
    component: About
  },
  {
    path: '/prendre-un-rendez-vous',
    name: 'Rdv',
    component: Rdv
  },
  
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/mon-compte',
    name: 'Compte',
    component: Compte
  },
  {
    path: '/mon-profil',
    name: 'Profil',
    component: Profil
  },
  {
    path: '/mes-animaux',
    name: 'ProfilAnimals',
    component: ProfilAnimals
  },
  {
    path: '/mes-rendez-vous',
    name: 'ProfilRdv',
    component: ProfilRdv
  },
  {
    path: '/inscription',
    name: 'Register',
    component: Register
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
 
router.beforeEach((to, from, next) => {
  if(to.path != '/login' && to.path != '/inscription'){  
    Vue.prototype.$axios.get('/session')
    .then(function (response) {
      // handle success
      //console.log(response);
      console.log(response.data);
      store.commit('SET_CLIENT_META', response.data);
      next();
    })
    .catch(function (error) {
      // handle error
      //console.log(error);
      //console.log("routing to login page");
      next({path: '/login'});
    })
}else{
  next();
}
});


export default router
