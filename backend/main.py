#!/usr/bin/env python
#-*- coding: utf-8 -*-


import os, sys, datetime,pymssql
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
from classes.Functions import Functions
from classes.Router import Router


##########################################################################
#  Backend site veto
##########################################################################
f = Functions()

#connexion à la base sql server
db = None
app = Flask(__name__)
app.config['ENV']= "development"
app.config['SECRET_KEY'] = '_0#tkj1x25sx"E3F0z\n\xec]/'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mssql+pymssql://vetoapp:1234@10.0.0.1:1433/vitrine_veto'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
dbA = SQLAlchemy(app)



router = Router(app=app,db=db,dbA=dbA,functions=f)
router.start()


app.run(
    host='0.0.0.0',
    port=5000,
    debug=True
) 

