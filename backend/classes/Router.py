import os, sys, datetime,pymssql,json,hashlib
from flask import Flask,request,session,Response,abort
from classes.Functions import Functions
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy.sql import text
from sqlalchemy import func

class Router:


    def __init__(self,app=None,db=None,dbA=None,functions=None):

        self.app = app
        self.db = db
        self.dbA = dbA
        self.functions= functions


    
    def start(self):
        
        app = self.app
        db = self.db
        dbA = self.dbA
        functions = self.functions

        Base = automap_base()
        Base.prepare(dbA.engine, reflect=True)


        Personne = Base.classes.Personne
        Specialite_veterinaire = Base.classes.Specialite_veterinaire
        Client = Base.classes.Client
        Animal =  Base.classes.Animal
        Type_animal = Base.classes.Type_animal
        Demande_rdv = Base.classes.Demande_rdv
        Veterinaire = Base.classes.Veterinaire

        ls = {
            "Personne": Personne,
            "Specialite_veterinaire": Specialite_veterinaire,
            "Client": Client,
            "Animal": Animal,
            "Type_animal":Type_animal,
            "Demande_rdv":Demande_rdv,
            "Veterinaire":Veterinaire
        }
        functions.setDBSession(dbA, ls)


        @app.errorhandler(404)
        def notFound(error):
            response = {
                "message": 'Ressource introuvable',
                "status": 404
            }
            return functions.objectToString(response),404

        #on vérifie avant chaque requête que l'id de l'utilisateur est dans session 
        @app.before_request
        def security_check():
            if request.path != "/login" and request.path != "/inscription" and request.method != "OPTIONS":
                try:
                    id = session["id"]
                except Exception as e:
                    abort(401)


        """Bloc exécuté aaprès le traitement de chaque requête Http
        -> Modification des entêtes de la réponse pour autoriser les requêtes cross-domaines (CORS) + traçage des requêtes.
        """                                      
        @app.after_request
        def afterEveryRequest(response):  
            # Modification des Headers pour autoriser les attributs spéciaux
            headers = request.headers.get('Access-Control-Request-Headers')
            if headers:
                response.headers['Access-Control-Allow-Headers'] = headers
            
            response.headers['Access-Control-Allow-Origin'] = request.environ.get('HTTP_ORIGIN')                
            response.headers['Access-Control-Allow-Credentials'] = 'true'        
            response.headers['Access-Control-Allow-Methods'] = 'GET, HEAD, POST, OPTIONS, PUT, PATCH, DELETE'  
            return response

        ######################################
        #routes concernant l'authentification#
        ######################################
        @app.route('/login', methods=['POST'])
        def login():
            try:
                data = request.get_json(force=True)       
            except Exception as e:
                print(e)
                abort(400)


            if functions.isset(data,['mail','password']) is False:
                print("Paramètres manquants")
                abort(400)
            
            if data["password"] is None or data["password"] == "":
                print("Password ne peut pas être null ou vide")
                abort(400)

            # on hash le password
            secret = hashlib.sha512(
                data['password'].encode('utf-8'), # Convert the password 
                ).hexdigest()


            # Requête pour vérifier si les identifiants fournies par l'utilisateur sont corrects
            personnes = dbA.session.query(Personne).filter(Personne.mail == data["mail"], Personne.password == secret).all()

            if len(personnes) != 1:
                print("Mot de passe ou identifiant incorret")
                abort(404) 
            personne = personnes[0]
            id_user = personne.id_personne

            # Requête pour vérifier si l'utilisateur est un client ou un veto
            types = dbA.session.query(Personne).join(Client).filter(Client.id_client == id_user).all()

            if len(types) != 1:
                type_user = "veto"
            else:
                type_user = "client"
            
            session["id"] = id_user
            session["type"] = type_user

            return functions.objectToString({"id": id_user, "type": type_user}), 200



        @app.route("/logout", methods=["GET"])
        def logout():
            del session["id"]
            del session["type"]
            return "ok", 200

        @app.route('/session', methods=['GET'])
        def get_session():
            try:
                id = session["id"]
            except Exception as e:
                print("Vous n'êtes pas authentifié")
                abort(401)
            if session["type"] == "client":
                info_personnes = dbA.session.query(Personne).join(Client).filter(Client.id_client == id).all()
                personne = functions.objectToDict(info_personnes[0]) 
                del personne["password"]
                personne["type"] = session["type"]             
                return functions.objectToString(personne)      
            else:
                
                _q = dbA.session.query(Personne,Veterinaire,Specialite_veterinaire).join(Veterinaire, Personne.id_personne == Veterinaire.id_veterinaire).join(Specialite_veterinaire, Specialite_veterinaire.id_spe == Veterinaire.id_specialite).filter(Veterinaire.id_veterinaire == id).all()
                data = _q[0]
                veto = functions.objectToDict(data[0])
                veto["avatar"] = data[1].avatar_compte
                veto["specialite"] = data[2].libelle_spe
                del veto["password"]
                veto['type'] = session['type']
                return functions.objectToString(veto)

            
        ################################
        #routes concernant les clients #
        ################################
        #
        #Route qui permet de lister les informations de tous les clients
        #
        @app.route('/clients',methods=["GET"])
        def list_clients():
            infos_clients = dbA.session.query(Personne).join(Client).all()
            data = []

            for c in infos_clients:
                data.append({
                    "id":c.id_personne,
                    "nom": c.nom_personne,
                    "prenom":c.prenom_personne,
                    "tel":c.tel,
                    "mail":c.mail,
                    "password":c.password,
                    "sexe":c.sexe
                    })

            return functions.objectToString(data)


        #
        #Route qui prend en paramètre l'id d'un client et affiche ses informations 
        # 
        @app.route('/client/<id>',methods=["GET"])
        def get_client(id):
            infos_client = dbA.session.query(Personne).join(Client).filter(Client.id_client == id).all()
            personne = functions.objectToDict(infos_client[0]) 
            


            return functions.objectToString(personne)

        #
        #Route qui permet de créer un client (nom, prenom, tel, mail, password,sexe)
        #
        @app.route('/client',methods=["POST"])
        def create_client():
            try:
                data = request.get_json(force=True)
            except Exception as e:
                print(e)
                abort(400)
            
            data['password'] = hashlib.sha512(
                data['password'].encode('utf-8'), # Convert the password 
                ).hexdigest()
            
            #insertion dans Personne
            query = text("""INSERT INTO Personne (
                nom_personne,prenom_personne,tel,mail,password,sexe
            ) VALUES(
                :nom_personne,:prenom_personne,:tel,:mail,:password,:sexe
            ) """)
            
            dbA.engine.execute(query,**data)

            #requête pour réupérer id_personne et l'insérer dans Client
            personne = dbA.session.query(Personne).filter(Personne.mail == data["mail"]).all()

            if len(personne) >0:
                #insertion dans la table client
                query = text("""INSERT INTO Client (
                    id_client,point_fidelite)
                    VALUES(
                        :id_client,10)"""
                )
            
                dbA.engine.execute(query,id_client=personne[0].id_personne)
            else:
                print("Personne introuvable")
                abort(500)
                
            return "ok", 201

        
        #
        #Route qui prend en paramètre l'id du client et modifie ses informations 
        #
        @app.route('/clients/<id>',methods=["PUT"])
        def update_client(id):
            try:
                data = request.get_json(force=True)
            except Exception as e:
                print(e)
                abort(400)
            
            # Récupération des infos du client et ajout des nouvelles données
            clients = dbA.session.query(Personne).join(Client).filter(Client.id_client == id).all()
            
            if len(clients) != 1:
                print("Le client à modifier est introuvable")
                abort(404) 
            client = clients[0]
            
            #parcours des nouvelles données pour remplacer les champs à modifier
            for attribute in data:
                if functions.isset(client,attribute) is True:
                    if attribute != "password":                           
                        setattr(client, attribute, data[attribute])
                    else:
                        if data["password"] != client.password:   # on vérifie si le password a été modifié par l'utilisateur,
                            new_password = hashlib.sha512(          # si c'est le cas on hash le new password et on l'ajoute
                                data['password'].encode('utf-8'), # Convert the password to bytes
                                ).hexdigest()
                            setattr(client, attribute, new_password)    
            
            
            #insertion dans la table client des infos modifiées
            dbA.session.add(client)
            #dbA.session.flush()
            dbA.session.commit() 
            return functions.objectToString(functions.objectToDict(client))
            #return "ok"

        
        
        ################################
        #routes concernant les animaux #
        ################################
        #
        #Route qui prend en paramètre l'id du client et liste ses animaux
        #
        @app.route('/clients/<id>/animals',methods=["GET"])
        def get_client_animals(id):
            infos_animal = dbA.session.query(Animal, Type_animal).join(Type_animal, Type_animal.id_type == Animal.id_type).filter(Animal.id_client == id ).all()
            data = []

            for info in infos_animal:
                animal = functions.objectToDict(info[0])
                animal["type"] = info[1].libelle_type
                try:
                    del animal["type_animal"]
                    del animal["client"]
                except Exception as e:
                    pass
                data.append(animal)
   
            return functions.objectToString(data)

        
        #
        #Route qui prend en paramètre l'id d'un animal, et affiche ses informations
        #
        @app.route('/animals/<id>',methods=["GET"])
        def get_animal(id):
            animal = dbA.session.query(Animal, Type_animal).join(Type_animal, Type_animal.id_type == Animal.id_type).filter(Animal.id_animal == id ).all()
            data = []
            
            for info in animal:
                animal = functions.objectToDict(info[0])
                animal["type"] = info[1].libelle_type
                data.append(animal)
            
            return functions.objectToString(data)


        #
        #Route qui crée un animal
        #
        @app.route('/animal',methods=["POST"])
        def create_animal():
            try:
                data = request.get_json(force=True)
            except Exception as e:
                print(e)
                abort(400)

            data["id_client"] = session["id"]

            #insertion dans Animal
            query = text("""INSERT INTO Animal (
                nom_animal,id_client,id_type,sexe,age,poids,taille
            ) VALUES(
                :nom_animal,:id_client,:id_type,:sexe,:age,:poids,:taille
            ) """)
            
            dbA.engine.execute(query,**data)

            return "Création de l'animal réussie"

            
        #
        #Route qui prend en paramètre l'id d'un animal et modifie ses infos
        #
        @app.route('/animal/<id>',methods=["PUT"])
        def update_animal(id):
            try:
                data = request.get_json(force=True)
            except Exception as e:
                print(e)
                abort(400)
            
            # Récupération des infos du client et ajout des nouvelles données
            animals = dbA.session.query(Animal).filter(Animal.id_animal == id ).all()

            if len(animals) != 1:
                print("L'animal à modifier est introuvable")
                abort(404) 
            animal = animals[0]

            #parcours des nouvelles données pour remplacer les champs à modifier
            for attribute in data:
                if functions.isset(animal,attribute) is True:                 
                    setattr(animal, attribute, data[attribute])
            
            update_animal = functions.objectToDict(animal)
            print(update_animal)
            
            #insertion dans la table client des infos modifiées
            
            dbA.session.add(animal)
            dbA.session.commit()
            return "Modification(s) réussie(s)"

             
        #
        #Route qui liste les types d'animaux disponibles
        #
        @app.route('/animal/types',methods=["GET"])
        def animal_types():
            list_type = dbA.session.query(Type_animal).all()
            data = []

            for t in list_type:
                data.append(functions.objectToDict(t))
            
            return functions.objectToString(data)


        #
        #Route qui prend en paramètre l'id d'un animal et le supprime
        #
        @app.route('/animal/<id>',methods=["DELETE"])
        def delete_animal(id):

            obj = dbA.session.query(Animal).filter(Animal.id_animal==id).first()

            if obj is None:
                print("L'animal à supprimer est introuvable")
                abort(404)
            dbA.session.delete(obj)
            dbA.session.commit()
            return "Suppresion réussie"
            
        ################################
        #routes concernant les RDV     #
        ################################

        #
        # Récupérer les N prochains jours du calendrier
        # Paramètres (query string):
        #   @start= point de départ (date) au format %d-%m-%Y
        #   @count= nombre de jours à renvoyer (int)
        #
        @app.route('/calendar/days', methods=["GET"])
        def getCalDays():

            start = request.args.get("start")
            count = request.args.get("count")

            if start is None or count is None:
                print('Paramètre manquant: start ou count.')
                abort(400)
            
            try:
                count = int(count)
                start = datetime.datetime.strptime(start, "%d-%m-%Y")
            except Exception as e:
                print("Erreur de conversion des paramètres")
                print(e)
                abort(400)
            
            frenchDays = {
                "Monday": "lundi",
                "Tuesday": "Mardi",
                "Wednesday": "Mercredi",
                "Thursday": "Jeudi",
                "Friday": " Vendredi",
                "Saturday": "Samedi",
                "Sunday": "Dimanche"
            }

            Days = [
                {
                    "date": start,
                    "date_string": start.strftime("%d/%m/%y"),
                    "date_libelle": frenchDays[start.strftime("%A")]
                }
            ]
            for i in range(count):

                start = start + datetime.timedelta(days = 1)
                day = {
                    "date": start,
                    "date_string": start.strftime("%d/%m/%y"),
                    "date_libelle": frenchDays[start.strftime("%A")]
                }
                Days.append(day)

            return functions.objectToString(Days)


        #
        # Récupérer les rdv à venir d'un véto.
        # Paramètres:                
        #
        @app.route('/calendar/veto/<string:id>', methods=["GET"])
        def getVetoRdv(id):
            
            list_rdv = dbA.session.query(Demande_rdv).filter(Demande_rdv.id_veto == id, Demande_rdv.date >= datetime.datetime.today()).all()
            data = []
            
            for rdv in list_rdv:
                rdv = functions.objectToDict(rdv)
                del rdv['client']
                del rdv['veterinaire']
                rdv['date'] = rdv["date"].strftime('%d/%m/%y')
                data.append(rdv)

            return functions.objectToString(data)

        #
        # Route pour récupérer les détails d'un rdv (désigné par son id)
        #        
        @app.route('/demandes_rdv/<string:id>', methods=['GET'])
        def getRdvById(id):

            _rdv = dbA.session.query(Demande_rdv,Personne,Animal).join(Personne, Personne.id_personne == Demande_rdv.id_client).join(Animal, Animal.id_animal == Demande_rdv.id_animal).filter(Demande_rdv.id_rdv == id).all()
            if len(_rdv) == 0:
                abort(404)

            data = _rdv[0]
            meta = functions.objectToDict(data[0])
            rdv = {
                "date": meta["date"].strftime('%m/%d/%Y'),
                "creneau": meta["creneau"],
                "client": data[1].prenom_personne + " " + data[1].nom_personne,
                "libelle": meta["libelle_rdv"],
                "animal": data[2].nom_animal,
                "statut": meta["statut"]
            }
            return functions.objectToString(rdv)


        #
        #Route qui prend en paramètre l'id d'un rdv et change son statut
        #
        @app.route('/demandes_rdv/<id>',methods=["PUT"])
        def update_rdv_status(id):
            try:
                data = request.get_json(force=True)
            except Exception as e:
                print(e)
                abort(400)

            list_rdv = dbA.session.query(Demande_rdv).filter(Demande_rdv.id_rdv == id).all()

            if len(list_rdv) != 1:
                print("Le rendez_vous à modifier est introuvable")
                abort(404) 
            rdv = list_rdv[0]
            
            #parcours des nouvelles données pour remplacer les champs à modifier
            for attribute in data:
                if functions.isset(rdv,attribute) is True:                 
                    setattr(rdv, attribute, data[attribute])
            
            update_rdv_status = functions.objectToDict(rdv)
            
            #insertion dans la table Demande_rdv des infos modifiées            
            dbA.session.add(rdv)
            dbA.session.commit()
           
            return functions.objectToString(update_rdv_status)


        #
        # Suppression d'une demande de RDV
        #
        @app.route("/demandes_rdv/<id>", methods=["DELETE"])
        def deleteRdv(id):
            obj = dbA.session.query(Demande_rdv).filter(Demande_rdv.id_rdv==id).first()

            if obj is None:
                print("Le rdv à supprimer est introuvable")
                abort(404)
            dbA.session.delete(obj)
            dbA.session.commit()
            return "Suppresion réussie"


        #    
        #Route qui permet de lister tous les rdv d'un client
        #
        @app.route('/demandes_rdv/<id>',methods=["GET"])
        def list_clients_rdv(id):
            list_rdv = dbA.session.query(Demande_rdv,Personne,Animal).join(Personne, Personne.id_personne == Demande_rdv.id_veto).join(Animal, Animal.id_animal == Demande_rdv.id_animal).filter(Demande_rdv.id_client == id, Demande_rdv.date >= datetime.datetime.today()).all()
            data = []
            
            for info in list_rdv:
                list_rdv = functions.objectToDict(info[0])
                rdv = {
                    "date": list_rdv["date"].strftime('%m/%d/%Y'),
                    "veto": "Dr. " + info[1].nom_personne,
                    "libelle": list_rdv["libelle_rdv"],
                    "animal": info[2].nom_animal
                }
                
                data.append(rdv)

                """list_rdv = functions.objectToDict(info[0])
                list_rdv["id_veto"] = info[1].nom_personne
                list_rdv["date"] = list_rdv["date"].strftime('%m/%d/%Y')
                data.append(list_rdv)"""
            
            return functions.objectToString(data)

        
        #
        #Route qui permet de créer un rdv
        #
        @app.route('/demandes_rdv',methods=["POST"])
        def create_rdv():
            try:
                data = request.get_json(force=True)
                data['date'] = datetime.datetime.strptime( data['date'], "%d/%m/%y")
            except Exception as e:
                print(e)
                abort(400)

            data["id_client"] = session["id"]
            data['statut'] = "non validee"           

            #insertion dans Demande_rdv
            query = text("""INSERT INTO Demande_rdv (
                libelle_rdv,date,id_client,id_veto,statut,creneau,id_animal
            ) VALUES(
                :libelle_rdv,:date,:id_client,:id_veto,:statut,:creneau,:id_animal
            ) """)
            
            dbA.engine.execute(query,**data)

            return "Création du rendez_vous réussie"

        
        ################################
        #routes concernant les vétos   #
        ################################

        #
        #Route qui liste les infos de tous les vetos
        #
        @app.route('/veterinaires/',methods=["GET"])
        def list_vetos():
            infos_vetos = dbA.session.query(Personne,Veterinaire,Specialite_veterinaire).join(Veterinaire, Personne.id_personne == Veterinaire.id_veterinaire).join(Specialite_veterinaire, Specialite_veterinaire.id_spe == Veterinaire.id_specialite).all()
            data = []

            for info in infos_vetos:
                veto = functions.objectToDict(info[0])
                veto["avatar"] = info[1].avatar_compte
                veto["specialite"] = info[2].libelle_spe
                del veto["password"]
                data.append(veto)

            return functions.objectToString(data)


        #
        #Route qui prend en paramètre l'id d'un véto et liste ses rdv à approuver
        #
        @app.route('/veterinaires/<id>',methods=["GET"])
        def list_approve_rdv(id):
            list_rdv = dbA.session.query(Demande_rdv,Personne).join(Personne, Personne.id_personne == Demande_rdv.id_client).filter(Demande_rdv.statut == "non_validee",Demande_rdv.id_veto == id).all()
            data = []
            
            for info in list_rdv:
                list_rdv = functions.objectToDict(info[0])
                list_rdv["id_client"] = info[1].nom_personne
                list_rdv["date"] = list_rdv["date"].strftime('%m/%d/%Y')
                data.append(list_rdv)
            
            return functions.objectToString(data)

        
        
