import os, sys, datetime,pymssql,json,re

##########################################################################
#  Script contenant les fonctions 
##########################################################################


class Functions:


    # Connexion à la base Sql server
    def connexion_db(self):

        try: 
            db = pymssql.connect(server="127.0.0.1", user="sa",password="1234", database="vitrine_veto", as_dict=True, charset="CP936")
            return db
            
        except Exception as e:
            print ("Erreur lors de la connexion à la base " + str(e.args))
            sys.exit(2)
            

    def objectToString(self,element):
        return json.dumps(element,default=self.jconverter,indent=4, sort_keys=True)
    
    def jconverter(self, o):
        if isinstance(o, datetime.datetime):
            return o.__str__()    

    def setDBSession(self, db, models):
        self.dbA = db
        self.models = models
    
    """
    Cette méthode vérifie que l'objet obj possède les attributs attr.
    Renvoie True ou False.
    """
    def isset(self, obj, attr):
        isObject = False
        if "sqlalchemy.ext.automap" in str(type(obj)):
            isObject = True
        try:
            if obj is None:
                return False
           
            if type(attr) is list:
                for a in attr:
                    try:
                        if isObject is True :
                            getattr(obj,a)
                        else:
                            test = obj[str(a)]            
                    except Exception:
                        return False               
            else:
                try:
                    if isObject is True :
                        getattr(obj,attr)
                    else:
                        test = obj[str(attr)]            
                except Exception:
                    return False
            return True
        except Exception as e:
            return False

    
    def objectToDict(self,obj):
        
        attrs = dir(obj)
        keys = []
        
        for attr in attrs:
            if "__" not in attr and "_decl" not in attr and "_collection" not in attr and "_sa" not in attr and attr not in ["prepare", "metadata", "classes"]:
                keys.append(attr)
        data = {}
        for key in keys:
            data[key] = getattr(obj,key)
        return data
